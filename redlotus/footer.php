<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Red_Lotus
 * @since Red Lotus 1.0
 */
?>

		</div><!-- #main -->
    </div><!-- #page-content -->

		<footer id="colophon" class="site-footer" role="contentinfo">

			<div class="site-info">
			  <?php get_sidebar( 'footer' ); ?>
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>
