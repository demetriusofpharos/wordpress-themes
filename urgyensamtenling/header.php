<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Urgyen_Samten_Ling
 * @since Urgyen Samten Ling 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
  <link rel="shortcut icon" href="/wp-content/themes/urgyensamtenling/images/UrgyenSamtenLing.ico" />
  <link href="/wp-content/themes/urgyensamtenling/css/urgyensamtenling.css" rel="stylesheet" type="text/css" />
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
    <div id="site-header-image">
      <img src="/wp-content/uploads/sites/2/2014/09/uslHeader.jpg" width="900" height="100" alt="Urgyen Samten Ling Gonpa" />
    </div>
  </a>
	<?php if ( get_header_image() ) : ?>
	<div id="site-header">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
			<img src="<?php header_image(); ?>" width="800" height="200" alt="">
		</a>
	</div>
	<?php endif; ?>

  <div id="page-content">
	<header id="masthead" class="site-header" role="banner">
		<div class="header-main">
			<nav id="primary-navigation" class="site-navigation primary-navigation" role="navigation">
				<button class="menu-toggle"><?php _e( 'Primary Menu', 'redlotus' ); ?></button>
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
			</nav>

			<div class="search-toggle">
				<a href="#search-container" class="screen-reader-text"><?php _e( 'Search', 'redlotus' ); ?></a>
			</div>
		</div>

		<div id="search-container" class="search-box-wrapper hide">
			<div class="search-box">
				<?php get_search_form(); ?>
			</div>
		</div>
	</header><!-- #masthead -->

	<div id="main" class="site-main">
