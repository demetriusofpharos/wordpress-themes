<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage SaltLakeCityGuitarLessons
 * @since Salt Lake City Guitar Lessons 2.0
 */
?>

		</div><!-- #main -->
    </div><!-- #page-content -->

    <footer id="colophon" class="site-footer" role="contentinfo">
      <div class="rsspad"><center><span class="rss"><a href="http://www.saltlakecityguitarlessons.com/feed" title="Subscribe to RSS"><abbr title="Subscribe to RSS">RSS</abbr></a></span></center></div>
      
      <center>Copyright &copy; <?php echo date("Y") ?> <a href="/" title="Salt Lake City Guitar Lessons">SaltLakeCityGuitarLessons.com</a>. All rights reserved.</center>
			<div class="site-info">
			  <?php get_sidebar( 'footer' ); ?>
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>
