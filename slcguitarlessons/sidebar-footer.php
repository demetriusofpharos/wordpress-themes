<?php
/**
 * The Footer Sidebar
 *
 * @package WordPress
 * @subpackage SaltLakeCityGuitarLessons
 * @since Salt Lake City Guitar Lessons 2.0
 */

if ( ! is_active_sidebar( 'sidebar-3' ) ) {
	return;
}
?>

<div id="supplementary">
	<div id="footer-sidebar" class="footer-sidebar widget-area" >
		<?php dynamic_sidebar( 'sidebar-3' ); ?>
	</div><!-- #footer-sidebar -->
</div><!-- #supplementary -->
