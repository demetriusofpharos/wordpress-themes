<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Guitar_University
 * @since Salt Lake City Guitar Lessons 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
  <link rel="shortcut icon" href="/wp-content/themes/slcguitarlessons/images/GuitarUniversity.ico" />
  <link href="/wp-content/themes/slcguitarlessons/css/slcguitarlessons.css" rel="stylesheet" type="text/css" />
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
  <header id="masthead" class="site-header" role="banner">
		<div class="header-main">
			<nav id="primary-navigation" class="site-navigation primary-navigation" role="navigation">
				<button class="menu-toggle"><?php _e( 'Primary Menu', 'slcguitarlessons' ); ?></button>
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
			</nav>
		</div>

    <div id="site-header-two">
      <div id="site-title">
        <span class="btitle">
          <a href="<?php echo home_url(); ?>/" title="Salt Lake City Guitar Lessons"><?php bloginfo('name'); ?></a>
        </span>

        <?php
          $description = get_bloginfo( 'description', 'display' );
          if ( ! empty ( $description ) ) :
        ?>
        <p><a href="/" title="<?php echo esc_html( $description ); ?>"><?php echo esc_html( $description ); ?></a></p>
        <?php endif; ?>
      </div>

      <!--<div id="image-rotator">
        <img src="/wp-content/themes/slcguitarlessons/images/bg-portrait0.jpg" />
      </div>-->

      <div id="site-header-image">
        <?php if ( get_header_image() ) : ?>
        <div id="site-header">
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="SaltLakeCityGuitarLessons.com" rel="home">
            <img src="<?php header_image(); ?>" alt="Salt Lake City Guitar Lessons">
          </a>
        </div>
        <?php endif; ?>
      </div>

      <nav id="secondary-nav" class="site-navigation secondary-nav" role="navigation">
        <ul>
           <li><a href="/lesson-types" title="Lesson Types">Lesson Types</a></li>
           <li><a href="/student-schedule" title="Student Schedule">Student Schedule</a></li>
           <li><a href="/policies-procedures" title="Policies &amp; Procedures">Policies &amp; Procedures</a></li>
           <li><a href="http://www.guitaruniversity.org" title="GuitarUniversity.org" target="blank">GuitarUniversity.org</a></li>
        </ul>
      </nav>
      
    </div>

	</header><!-- #masthead -->

  
  <div id="page-content">
	
	<div id="main" class="site-main">
