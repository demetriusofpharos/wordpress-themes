<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Guitar_University
 * @since Guitar University 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
  <link rel="shortcut icon" href="/wp-content/themes/guitaruniversity/images/GuitarUniversity.ico" />
  <link href="/wp-content/themes/guitaruniversity/css/guitaruniversity.css" rel="stylesheet" type="text/css" />
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
  <div id="site-header-image">
    <?php if ( get_header_image() ) : ?>
    <div id="site-header">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="GuitarUniversity.org" rel="home">
        <img src="<?php header_image(); ?>" alt="Guitar University">
      </a>
    </div>
    <?php endif; ?>
  </div>

  <div id="page-content">
	<header id="masthead" class="site-header" role="banner">
		<div class="header-main">
			<div class="search-toggle">
				<a href="#search-container" class="screen-reader-text"><?php _e( 'Search', 'guitaruniversity' ); ?></a>
			</div>

			<nav id="primary-navigation" class="site-navigation primary-navigation" role="navigation">
				<button class="menu-toggle"><?php _e( 'Primary Menu', 'guitaruniversity' ); ?></button>
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
			</nav>
		</div>

		<div id="search-container" class="search-box-wrapper hide">
			<div class="search-box">
				<?php get_search_form(); ?>
			</div>
		</div>
	</header><!-- #masthead -->

	<div id="main" class="site-main">
