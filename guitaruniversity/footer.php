<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Guitar_University
 * @since Guitar University 1.0
 */
?>

		</div><!-- #main -->
    </div><!-- #page-content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
    <center>Copyright &copy; 2016-<?php echo date("Y") ?> GuitarUniversity.Org</center>
			<div class="site-info">
			  <?php get_sidebar( 'footer' ); ?>
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>
